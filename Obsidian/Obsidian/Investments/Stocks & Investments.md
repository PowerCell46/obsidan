# IPOs

## Initial Public Offering

process by which a private company offers its shares to the public for the first time on a stock exchange. This marks the transition of a company from being privately held to publicly traded.
