
```python
import requests  
from bs4 import BeautifulSoup  
  
print(requests.get('https://www.scrapethissite.com/pages/simple/').text)  
  
countries_page = requests.get('https://www.scrapethissite.com/pages/simple/').content  
  
parser = BeautifulSoup(countries_page, 'html.parser')  
  
print(parser.find(id="countries").prettify())
```
