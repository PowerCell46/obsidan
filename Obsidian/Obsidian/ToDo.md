# Courses

## Python ORM
1. Exam
## Django Basics
10. Forms Advanced
13. Classed Based Views Advanced

# Java Fundamentals

Lists Ex: Anonymous Threat: 
https://alpha.judge.softuni.org/contests/lists-exercise/1297/compete#7
2. More exercises tasks
3. Regex all tasks

# Java Advanced
Math Potato:
https://alpha.judge.softuni.org/contests/stacks-and-queues-lab/1437/practice#6
Search History Update:
https://alpha.judge.softuni.org/contests/stacks-and-queues-lab/1437/practice#7
Recursive Fibonacci:
https://alpha.judge.softuni.org/contests/stacks-and-queues-exercises/1442/compete#5

Voina - Number game
https://alpha.judge.softuni.org/contests/sets-and-maps-advanced-lab/1462/practice#2
1. Starred Tasks: 
	- Stacks and Queues Exercise 
2. Iterators and Comparators
3. Exam/s
4. Workshop algorithms

# Java OOP
1. Exam/s
2. Finish the rest of the tasks
3. Finish the rest of the lectures


#### https://fmi.github.io/java-course/

__________________________________________________________________________
# Topics for learning (Long Term)

ansi sql
redux

Depth-First Search (DFS)
Breadth-First Search (BFS)

binary tree
balanced tree
AVL tree

open address
chaining
load factor
https://www.elastic.co/elasticsearch
java labels
java pairs
reference method in java
priority queue java
boxed java
https://www.youtube.com/results?search_query=generics+and+wildcards+in+java

Stack and Heap
Bitwise Operations

java lang 
god mode classes
Sealed classes (permits ...)
(Kafka)

hammer principle
yagni principle
kiss principle

Spring Profiles - за различни ДБ конфигурации
@Audited
ControllerAdvice 
chrome://topics-internals/
sublime text
https://d3js.org/
Sprint (Software development)
TSV file
Apache JMeter - performance testing

https://cartography.bg/index.php/resources-in-english/
https://cartography.bg/wp-content/uploads/2023/08/Beyond-the-Map-Exploring-the-Boundaries-of-Communication-in-Contemporary-Cartography-Data-Visualization.pdf

context window size:
The context window size refers to the maximum number of tokens the model can process and "remember" in one go.

https://judge.openfmi.net/

alloc malloc 
concurrency support
move semantics
constexpr
report builder React

wei gwei

@ToString(exclude = {"powerPlants","energyBalancingContracts"})

emplace_back: https://cplusplus.com/reference/vector/vector/emplace_back/
CDR file format
move constructor and assignment - for C++11 move semantics
size_t
little endian system
SSL Certificate
Сини чипове

1. Balance Sheet
2. total current assets / total current liabilities
3. Income statement
4. (operating income / total revenue) * 100 (ideally above 15%)
5. statement cashflow  

dollar cost averaging
smart pointers:  
- weak pointer    
- shared pointer    
- unique pointer
Задачата от сашо в Инстаграм за матриците
C++ DB connection
C++ records
OCR
query dsl
operators < > with set using the class

const pointers 
const references
random numbers
Анализът на граничните стойности

wsl - windows subsystem for linux

Защо другите езици нямат видимост на функциите ако са декларирани по-долу, а не като джава скрипт
llvm: https://aosabook.org/en/v1/llvm.html
https://opencv.org/
mv hello.txt welcome.txt -> rename file

да го "отворя"
Да ви дам моето ИП  
да ви кажа че порт 7000 е овторен за inbound/outbound traffic
да го достъпите

In code development, **concretion** refers to the specific implementation of an abstraction.
- **Abstraction:** `Database.h`
- **Concretion:** `MySQLDatabase.cpp`
https://www.geeksforgeeks.org/how-to-use-string_agg-to-concatenate-strings-in-sql-server/

__________________________________________________________________________
# Books

thinking in java
effective java
clean code книга
how to lie with maps

__________________________________________________________________________
# Research

Паучи (Baozi)
nikuman 

__________________________________________________________________________
# Watch List
 
- The It Crowd

__________________________________________________________________________
# C++ Notes

casts (dynamic const reinterpret static)
smart pointers

___
### TODO Libraries

js babel
jszip


https://en.cppreference.com/w/cpp/string/basic_string_view


Ai website design:
https://axolotl.ai/




move the lib and header files to the project

  
// use moment() library

### https://www.plus500.com/bg/


# C++ Database Project

https://chatgpt.com/share/67afcc2f-26d4-8003-989c-6c2b9b7c052f

1. Add Where clauses (write the implementation of =, IN etc...)

first solar inc

# Leetcode


___

https://leetcode.com/problems/queries-quality-and-percentage/description/
https://leetcode.com/problems/sales-analysis-iii/submissions/1553665756/
https://leetcode.com/problems/reformat-department-table/

Агремеент тайпс по дифаоулл всичко да е селектирано
Повер Плантс по дифоулт всичко да е селектирано

text-overflow ellipsis

```cpp
// For access through an iterator or pointer, we use the operator ->  
for (auto iter = people.begin(); iter != people.end(); ++iter)  
    std::cout << iter->getName() << ' ' << iter->getAge() << '\n';  
    // std::cout << *iter << '\n';
```



A C D -> dannite za попълване в струнга
P - полето за връзка

std list
