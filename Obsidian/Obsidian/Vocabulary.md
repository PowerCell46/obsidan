
| Foreign Word            | Explanation                                    |
| ----------------------- | ---------------------------------------------- |
| thyme                   | мащерка                                        |
| n/a = not applicable    | when you cannot give a relevant answer to a qu |
| Queen dowager           | Кралица вдовица                                |
| interloper              |                                                |
| subterfuge              | a trick or a dishonest way of achievi          |
| contempt of court       | неуважение към съда                            |
| come hell or high water | You are determined to do something, despite t  |
| eye of the beholder     | a matter o                                     |
| concurrent              |                                                |
| beyond the pale         |                                                |
| plight                  | unpleasant condition, especially seriou        |
| abridged                | made shorter by ha                             |
| Бруто                   |                                                |
| Нето                    | Заплатата, след пр                             |
| indict                  |                                                |
| Tootski                 |                                                |
| acidic                  |                                                |
| adderall                |                                                |
| futile                  |                                                |
| deciduous               | широколистен                                   |
| disparage               | омаловажаван                                   |
| frat                    | братство                                       |
| adjacent                | съседен                                        |
 